import QtQuick 2.15
import QtQuick.Controls 2.15

Rectangle {
    id: my_rect
    property bool isBlue: true
    color: isBlue ? "blue" : "red"
    MouseArea {
        anchors.fill: parent
        onClicked: {
            my_rect.isBlue = !my_rect.isBlue
            python.outputStr("rect")
        }
    }
    Button {
        id: my_button
        x: 0
        y: 0
        anchors.fill: parent
        anchors.topMargin: 70
        anchors.bottomMargin: 70
        text: qsTr(python.btnText)
        onClicked: {
            python.outputStr("btn")
        }
    }
}
