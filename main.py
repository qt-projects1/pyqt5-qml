import os
import sys
from PyQt5 import QtCore, QtWidgets, QtQuick
from pathlib import Path


class CustomQuickView(QtQuick.QQuickView):
    def __init__(self, path, ctx_name, parent=None):
        super().__init__(parent)
        self.statusChanged.connect(self.on_statusChanged)
        self.setResizeMode(QtQuick.QQuickView.SizeRootObjectToView)
        context = self.rootContext()
        context.setContextProperty(ctx_name, self)  # Expose the object to QML.
        qml_file = os.fspath(Path(__file__).resolve().parent / 'qml' / path)
        self.setSource(QtCore.QUrl.fromLocalFile(qml_file))

    def on_statusChanged(self, status):
        if status == QtQuick.QQuickView.Error:
            for error in self.view.errors():
                print(error.toString())
            sys.exit(-1)

    @QtCore.pyqtProperty(str)
    def btnText(self):
        return 'my_button'

    @QtCore.pyqtSlot(str)
    def outputStr(self, s):
        print(s)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        super().__init__()
        self.resize(300, 200)
        central = QtWidgets.QWidget(self)
        central.setLayout(QtWidgets.QHBoxLayout())
        self.setCentralWidget(central)

        self.view = CustomQuickView(path='component.qml', ctx_name="python")

        self.container = central.createWindowContainer(self.view)
        central.layout().addWidget(self.container)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()

    res = app.exec_()

    sys.exit(res)
